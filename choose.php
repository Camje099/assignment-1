<html>

<head><title>TimeZone list</title>
<style>
th {text-align: center; }
table, th, td {
	border:2px solid blue;
}
th, td {
	padding :0.3em;
}
</style>
</head>

<p> Welcome, <?php  echo $_POST["name"]; ?></p>

<body>

<h1>List of available Timezones</h1>

<table border="1">
<tr><th>Name</th><th>Timezone</th><th>Difference from UTC</td></tr>

<?php

$t = $_POST["code"];

if($t == '4420'){
	echo "You are using Ted's code";
} else if ($t == '6167'){
	echo "You are using Phil's code";
} else if ($t == '2101'){
	echo "You are using Steve's code";
}else if($t == '6400'){
	echo "You are using Rick's code";
}else if ($t == '5173'){
	echo "You are using Josh's code";
} else if ($t == '1985'){
	echo "You are using Jerry's code";
} else {
	echo "You are operating as a guest";
}

$servername = '192.168.4.12';
$username = 'webuser';
$password = 'insecure_db_pw';
$dbname = 'places';

$pdo_dsn = "mysql:host=$servername;dbname=$dbname";

$pdo = new PDO($pdo_dsn, $username, $password);

$q = $pdo->query("SELECT * FROM locations");

while ($row = $q->fetch()){
 echo "<tr><td>".$row["name"]."</td><td>".$row["timezone"]."</td><td>".$row["diff"]."</td></tr>\n";
}

?>

</table>
</body>
</html>
