CREATE TABLE users (
   name varchar(50),
   code varchar(5),
   PRIMARY KEY (name)
);

INSERT INTO users VALUES ('Ted', '4420');
INSERT INTO users VALUES ('Phil', '6167');
INSERT INTO users VALUES ('Steve', '2102');
INSERT INTO users VALUES ('Rick', '6400');
INSERT INTO users VALUES ('Josh', '5173');
INSERT INTO users VALUES ('Jerry', '1985');
