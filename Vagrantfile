# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile for creating a database server machine and a 
# webserver machine for a Timezone conversion application

Vagrant.configure("2") do |config|
  # Use the ubuntu OS
  config.vm.box = "ubuntu/xenial64"

  # Configuration specifications for the webserver VM
  config.vm.define "webserver" do |webserver|
    # Set webserver vm hostname to webserver
    webserver.vm.hostname = "webserver"
    
    # Host computer can connect to IP address 127.0.0.1 port 8081, 
    # and network requests will reach our webserver VM's port 80.

    webserver.vm.network "forwarded_port", guest: 80, host: 8081, host_ip: "127.0.0.1"
    
    # The vms will communicate on a private network called 'private_network', 
    # each VM will have an IP of 192.168.2.x, with x being 11, 12 or 13.

    webserver.vm.network "private_network", ip: "192.168.4.11"

    # Creates the shared folder between the VM and host machine
    # Not fatally important for this but couldn't hurt.
    webserver.vm.synced_folder ".", "/vagrant", owner: "vagrant", group: "vagrant", mount_options: ["dmode=775,fmode=777"]

    # Now we have a section specifying the shell commands to provision
    # the webserver VM. 
    webserver.vm.provision "shell", inline: <<-SHELL
      apt-get update
      apt-get install -y apache2 php libapache2-mod-php php-mysql
      
      # copy the correct webpage html into the webserver VM
      cp /vagrant/net/index.html ../../var/www/html
      cp /vagrant/net/choose.php ../../var/www/html
      cp /vagrant/net/add_to_db.php ../../var/www/html

      # Reload the webserver configuration, to pick up our changes
      service apache2 reload
    SHELL
  end

  # Here is the section for defining the database server 'dbserver'
  config.vm.define "dbserver" do |dbserver|
    dbserver.vm.hostname = "dbserver"
    # The database server IP on the private_network is 192.168.4.12
    dbserver.vm.network "private_network", ip: "192.168.4.12"
    
    # Setup the shared folder between VM and Host
    dbserver.vm.synced_folder ".", "/vagrant", owner: "vagrant", group: "vagrant", mount_options: ["dmode=775,fmode=777"]
   
    # Shell provisioning commands for Database server
    dbserver.vm.provision "shell", inline: <<-SHELL
      
      # Update Ubuntu software packages.
      apt-get update
      
      # Create a shell variable MYSQL_PWD that contains the MySQL root password
      export MYSQL_PWD='insecure_mysqlroot_pw'

      echo "mysql-server mysql-server/root_password password $MYSQL_PWD" | debconf-set-selections 
      echo "mysql-server mysql-server/root_password_again password $MYSQL_PWD" | debconf-set-selections

      # Install the MySQL database server.
      apt-get -y install mysql-server

      # Run some setup commands to get the database ready to use.
      # First create a database.
      echo "CREATE DATABASE places;" | mysql

      # Then create a database user "webuser" with the given password.
      echo "CREATE USER 'webuser'@'%' IDENTIFIED BY 'insecure_db_pw';" | mysql
      

      # Grant all permissions to the database user "webuser" regarding
      # the "login" database that we just created, above.
      echo "GRANT ALL PRIVILEGES ON places.* TO 'webuser'@'%'" | mysql


      # Set the MYSQL_PWD shell variable that the mysql command will
      # try to use as the database password ...
      export MYSQL_PWD='insecure_db_pw'

      # ... and run all of the SQL within the setup-database.sql file,
      # which is part of the repository containing this Vagrantfile, so you
      # can look at the file on your host. The mysql command specifies both
      # the user to connect as (webuser) and the database to use (places).
      cat /vagrant/setup-database.sql | mysql -u webuser places

      # By default, MySQL only listens for local network requests,
      # i.e., that originate from within the dbserver VM. We need to
      # change this so that the webserver VM can connect to the
      # database on the dbserver VM. Use of `sed` is pretty obscure,
      # but the net effect of the command is to find the line
      # containing "bind-address" within the given `mysqld.cnf`
      # configuration file and then to change "127.0.0.1" (meaning
      # local only) to "0.0.0.0" (meaning accept connections from any
      # network interface).
      sed -i'' -e '/bind-address/s/127.0.0.1/0.0.0.0/' /etc/mysql/mysql.conf.d/mysqld.cnf

      # We then restart the MySQL server to ensure that it picks up
      # our configuration changes.
      service mysql restart
    SHELL
  end

  config.vm.define "bqserver" do |bqserver|
      bqserver.vm.hostname = "bqserver"
	
      bqserver.vm.network "private_network", ip:"192.168.4.13"
      bqserver.vm.synced_folder ".", "/vagrant", owner: "vagrant", group: "vagrant", mount_options: ["dmode=775,fmode=777"]

      
      bqserver.vm.provision "shell", inline: <<-SHELL
	
	#update Unbuntu software
	apt-get update
      
	export MYSQL_PWD='insecure_mysqlroot_pw'

  	echo "mysql-server mysql-server/root_password password $MYSQL_PWD" | debconf-set-selections 
        echo "mysql-server mysql-server/root_password_again password $MYSQL_PWD" | debconf-set-selections

	#Install the MySQL database server
        apt-get -y install mysql-server
	apt-get -y install expect

        #Set up the database
        echo "CREATE DATABASE login;" | mysql

        #Then create a database user "web user" with the given password
        echo "CREATE USER 'webuser'@'%' IDENTIFIED BY 'insecure_db_pw';" | mysql

        #Grant all permissions to 'webuser'
        echo "GRANT ALL PRIVILEGES ON login.* TO 'webuser'@'%'" | mysql
     
        #Set mysql password
        echo MYSQL_PWD='insecure_db_pw'
   
        #copy database setup file into VM
        cat /vagrant/setup-databases.sql | mysql -u webuser -p login

	

	sed -i'' -e '/bind-address/s/127.0.0.1/0.0.0.0/' /etc/mysql/mysql.conf.d/mysqld.cnf	

	service mysql restart     

	 
     SHELL
  end	
end

#  LocalWords:  webserver xenial64
