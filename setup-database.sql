CREATE TABLE locations (
   name varchar(50),
   timezone varchar(5),
   diff varchar(5),
   PRIMARY KEY (name)
);

INSERT INTO locations VALUES ('Australia', 'LINT', '+14');
INSERT INTO locations VALUES ('Brazil', 'BRT', '-3');
INSERT INTO locations VALUES ('Canada', 'PDT','-9');
INSERT INTO locations VALUES ('Chile', 'EAST','-7');
INSERT INTO locations VALUES ('China', 'CST', '+8');
INSERT INTO locations VALUES ('India', 'IST', '+6');
INSERT INTO locations VALUES ('New Zealand', 'NZST', '+12');
INSERT INTO locations VALUES ('Peru', 'PET','-5');
INSERT INTO locations VALUES ('Samoa', 'WST','+13');
INSERT INTO locations VALUES ('Singapore', 'SGT','+7');
INSERT INTO locations VALUES ('South Africa', 'SAST','+2');
INSERT INTO locations VALUES ('Taiwan', 'CST','+8');
INSERT INTO locations VALUES ('United Kingdom', 'BST', '-4');
INSERT INTO locations VALUES ('United States', 'CDT', '-6');
INSERT INTO locations VALUES ('Vanuatu', 'VUT', '+11');
